#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <wait.h>
#include <dirent.h> 



void delete_directory(char *directory)
{
    pid_t child_id = fork();
	int status;

	if (child_id < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id == 0) 
    {
    	char *argv[] = {"rm","-r" ,directory, NULL};
    	execv("/bin/rm", argv);
	}
	else 
    {
    	while((wait(&status)) > 0);
    	return;
	}
}

void make_directory(char *directory){
    pid_t child_id_m = fork();
	int statuss;

	if (child_id_m < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id_m == 0) 
    {
    	char *argv[] = {"mkdir", "-p", directory, NULL};
        execv("/bin/mkdir", argv);

	}
	else 
    {
    	while((wait(&statuss)) > 0);
    	return;
	}
}

void list_filename()
{
    int i = 0;
    char namafilm[6][20];
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d) 
    {
        while ((dir = readdir(d)) != NULL) 
        {


        }
        closedir(d);
    }
}


int main ()
{
//Sequence Counter
int seqcounter = 0;

//sequence 1
while (seqcounter == 0)
    {
        pid_t child_id;
        // Creating  child
        child_id = fork();
        int status;


        if (child_id < 0)
        {
            exit(EXIT_FAILURE); 
        }

        if (child_id == 0) 
        {
            chdir ("/home/hemakesha");
            make_directory("drakor");
            chdir ("/drakor");

            char filePath[] = "/home/hemakesha/";
            char fileName[] = "drakor.zip";
            char projectPath[] = "/home/hemakesha/drakor/";
            strcat(filePath,fileName);
            char *argv[] = {"unzip", filePath, "-d", projectPath, NULL};
            execv("/usr/bin/unzip",argv);
        }

        else 
        {

            while ((wait(&status)) > 0);
            chdir ("/home/hemakesha/drakor");
            delete_directory("coding");
            delete_directory("song");
            delete_directory("trash");
            seqcounter++;
        }
    }


while  (seqcounter == 1)
    {
        pid_t child_id_2;
        // Creating  child
        child_id_2 = fork();
        int status_2;

        if (child_id_2 < 0)
        {
            exit(EXIT_FAILURE); 
        }  

        if (child_id_2 == 0) 
        {
            chdir ("/home/hemakesha/drakor");
            make_directory("thriller");
            make_directory("action");
            make_directory("romance");
            make_directory("horror");
            make_directory("school");
            make_directory("comedy");
            make_directory("fantasy");
        }

        else
        {
            chdir ("/home/hemakesha/drakor");
 
            // Keep printing tokens while one of the
            // delimiters present in str[].
            //read through list
            //usestrtok to rename2
            seqcounter++;
            break;
        }  
    }

  return 0;
}
