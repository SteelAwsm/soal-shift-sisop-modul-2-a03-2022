# Laporan Penjelasan dan Penyelesaian Soal

## Kelompok A03

| Nama          | NRP           |
|---------------|---------------|
|James Silaban                  | 5025201169|
|Hemakesha Ramadhani Heriqbaldi | 5025201209|
|Wina Tungmiharja               | 5025201242| 


# Soal 1

## Membuat Template Daemon
```
int main(){
  pid_t process_id = 0;
  pid_t sid = 0;
  process_id = fork();

  if (process_id < 0 || process_id > 0) {
    exit(1);
  }

  umask(0);
  sid = setsid();
  if (sid < 0) {
    exit(1);
  }

  char projectPath[80];
  strcpy(projectPath, "/home/wina/Documents/soal-shift-sisop-modul-2-a03-2022/soal1/");
  chdir(projectPath);
  
  while(1){
    ...
  }
}
```

disini kita melakukan dfeklarasi `projectPath` agar semua pembuatan file dan folder akan dilakukan di folder yang sudah ditentukan

## a. Mendownload FIle characters dan file weapons dari Google Drive

**Membuat folder dengan nama `gacha-gacha`**

Sebelum melakukan download file, maka akan dibuat sebuah _child process_ untuk membuat folder tersebut dengan funggsi `make_directory("gacha_gacha")`

```
void make_directory(char *directory){
  char *argv[] = {"mkdir", "-p", directory, NULL};
  execv("/bin/mkdir", argv);
}
```
- penjelasan:
fungsi `make_directory` akan menerima satu parameter yaitu nama folder yangg dibuat, Setelah itu akan dipanggil commmand `execv` dengan 

command `mkdir` untuk membuat directory
command `-p` untuk membuat directory jika belum ada

- eksekusi: 
```
pid_t child_id;
int status;

child_id = fork();

if (child_id < 0) {
  exit(EXIT_FAILURE);
}
if (child_id == 0) {
  make_directory("gacha_gacha");
}
else {
...
}
```

**Men_download_ File dan Melakukan _unzip_ dari Google Drive`**

Sebelum melakukan _download_ dan _unzip_, kita melakukan deklarasi terlebih dahulu _link_ google drive dan nama folder ke dalam _array_ sebagai berikut

```
char *drive_character[] = {
  "https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","characters.zip"
};
char *drive_weapon[] = {
  "https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "weapons.zip"
};
```

Untuk melakukan download dan _unzip_ file, maka kita harus membuat proses yang saling menunggu antara _Child Process_ dan _Parent Process_ dengan metode sekuensial. Oleh karena itu, disini kita akan membuat proses baru lagi yang melakukan _download_ dan _unzip_ file

```
void download_and_zip (char *source, char *projectPath, char *fileName){
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0) {
  
    char filePath[150];
    strcpy(filePath,projectPath);
    strcat(filePath,fileName);
    char *argv[] = {"wget","--no-check-certificate", source, "-O", filePath, NULL};
    execv("/usr/bin/wget",argv);
    
  } else {
  
    while ((wait(&status)) > 0);
    
    char filePath[150];
    strcpy(filePath,projectPath);
    strcat(filePath,fileName);
    char *argv[] = {"unzip", filePath, "-d", projectPath, NULL};
    execv("/usr/bin/unzip",argv);
    
  }
}
```
- penjelasan:
fungsi `wget` akan melakukan download dari URL yang tersedia, kemudian dipangggil dengan comman `execv` 
command `-O` untuk menyimpan hasil unduhan denggan nama file yang sudah kita tentukan sebelumnya

funggsi `unzip` dilakukan untuk melakukan unzip file .zip
command `-d` untuk menyimpan file .zip sesuai dengan direktori yang sudah kita tentukan

- eksekusi: 
setelah kita membuat fungsi tersebut, maka kita akan memanggil fungsi yang akan mendownload kedua file characters dan weapon 
```
void download_data(char *projectPath){
  pid_t download_id;
  int status_download;
  download_id = fork();
  if (download_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }
  if (download_id == 0) {
    download_and_zip(drive_character[0],projectPath,drive_character[1]);
  } else {
    while ((wait(&status_download)) > 0);
    download_and_zip(drive_weapon[0],projectPath,drive_weapon[1]);
  }
}
```

dan kita akan memanggil fungsi tersebut di `main()` dengan eksekusi sebagai berikut
```
...
else {
  pid_t child_id_1;
  int status_1;

  child_id_1 = fork();

  if (child_id_1 < 0) {
    exit(EXIT_FAILURE);
  }
  if (child_id_1 == 0) {
    download_data(projectPath);
  } else {
    ...
  }
}
```

## b. Melakukan Gacha

**Membaca direktori _Characters_ dan _Weapon_ dan menyimpan nama file ke dalam array**

Sebelum melakukan pembacaan direktori, di deklarasikan variable global seperti berikut

```
char characters_file[500][500];
int characters_count=0;

char weapons_file[500][500];
int weapons_count=0;
```
- penjelasan:
array `characters_file` dan `weapons_file` akan menyimpan nama file .json yang terdapat pada folder yang telah di unzip. Tujuan dari hal ini adalah memudahkan kita untuk mengakses file tersebut dari index pada array ketika melakukan gacha

'characters_count' dan `weapons_count` akan menyimpan berapa banyak file dalam direkorti yangg telah di unzip jadi kita akn diberikan informasi seberapa banyak angka yang harus dipilih ketika melakukan gacha nanti.

- eksekusi: 
disini kita akan menggunakan `read_weapons_directory` dan `read_weapons_directory` untuk melakukan pembacaan direktori tersebut
```
void read_weapons_directory(){
  DIR *directory;
  struct dirent *en;
  directory = opendir("weapons");
  while ((en = readdir(directory)) != NULL) { 
      strcpy(weapons_file[weapons_count],en->d_name);
      weapons_count++;
  }
  closedir(directory);
}

void read_characters_directory(){
  characters_count=0;
  DIR *directory_a;
  struct dirent *en_a;
  directory_a = opendir("characters");
  while ((en_a = readdir(directory_a)) != NULL) {
    strcpy(characters_file[characters_count],en_a->d_name);
    characters_count++;
  }
  closedir(directory_a);
}
```

**Melakukan Gacha sesuai dengan Format yang telah ditentukan**

Karena disini, folder yang dimasukkan hasil gacha nya akan berubah setiap hasil `gacha % 90` maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut, oleh karena itu, setiap kali hasil gacha memenuhi angka tersebut, hasil selanjutnya akan kita lakukan di folder yangg baru

oleh karena itu dapat ditulis sebagai berikut

```
char nama_folder[200];
char nama_file[200];

int file_num = 0;
int folder_num = 0;

int i = 1;
int r;

while(...){
  if(i%10==1){
    file_num+=10;
    snprintf(nama_file, 200, "%d:%d:%d_gacha_%d", localtime(&now)->tm_hour, localtime(&now)->tm_min, localtime(&now)->tm_sec, file_num); 
  } 
  if(i%90==1){
    folder_num+=90;
    snprintf(nama_folder, 200, "total_gacha_%d", folder_num); 
    make_folder_gacha(nama_folder);
  }
}
```

setelah itu, setiap kali gacha bernilai genap, maka akan dilakukan gacha weapon, dan sebaliknya jika gacha bernilai ganjil, maka akan dilakukan gacha characters
```
if(i%2==0){
  r = rand() % weapons_count;
  get_weapons_gacha(r,i,primogems, nama_folder, nama_file);
} else {
  r = rand() % characters_count;
  get_characters_gacha(r,i,primogems, nama_folder, nama_file);
}
```

## c. Menulis ke file .txt dan memasukkan ke folder

**Membuat Fungsi untuk Menulis Hasil Gacha**

Sebelum melakukan gacha, maka akan dibuat fungsi untuk melakukan gacha _characters_
 dan gacha _weapon_ sebagai berikut
```
void get_characters_gacha(int random_number, int gacha_count, int remain_primogems, char *nama_folder, char *nama_file){
  FILE *f;
  char buffer[1000000];

  struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *rarity;

  char directory[100] = "characters/";
  strcat(directory,characters_file[random_number]);

  f = fopen(directory,"r");
  fread(buffer, 1000000, 1, f);
  fclose(f);
  parsed_json = json_tokener_parse(buffer);
  json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);

  char write_directory[500];
  strcpy(write_directory,"gacha_gacha");
  strcat(write_directory,"/");
  strcat(write_directory,nama_folder);
  strcat(write_directory,"/");
  strcat(write_directory,nama_file);
  strcat(write_directory, ".txt");
  
  FILE *txt = fopen(write_directory, "a");
  fprintf(txt, "%d_characters_%d_%s_%d\n",gacha_count, json_object_get_int(rarity), json_object_get_string(name), remain_primogems);
  fclose(txt);
}

void get_weapons_gacha(int random_number, int gacha_count, int remain_primogems, char *nama_folder, char *nama_file){
  FILE *f;
  char buffer[5000000];

  struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *rarity;

  char directory[100] = "weapons/";
  strcat(directory,weapons_file[random_number]);

  f = fopen(directory,"r");
  fread(buffer, 5000000, 1, f);
  fclose(f);
  parsed_json = json_tokener_parse(buffer);
  json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);

  char write_directory[500];
  strcpy(write_directory,"gacha_gacha");
  strcat(write_directory,"/");
  strcat(write_directory,nama_folder);
  strcat(write_directory,"/");
  strcat(write_directory,nama_file);
  strcat(write_directory, ".txt");
  
  FILE *txt = fopen(write_directory, "a");
  fprintf(txt, "%d_weapons_%d_%s_%d\n",gacha_count, json_object_get_int(rarity), json_object_get_string(name), remain_primogems);
  fclose(txt);
}
```
- penjelasan:
disini kita membuat dua fungsi identik dengan fungsi yang berbeda, yaitu buat _characters_ dan _weapon_, disini kita juga melakukan passing variabel sebagai berikut

`int random_number` : angka ini yang akan mempresentasikan item ke-berapa yang akan diambil
`int gacha_count` : abgka ini menunjukkan jumlah gacha yang telah dilakukan
`int remain_primogems` : angka ini menunjukkan sisa primogems
`char *nama_folder` : menunjukkan dimana folder direktori akan ditulis
`char *nama_file` : menunjukkan dimana file `.txt` gacha akan ditulis

didalam fungsi tersebut, akan dilakukan pembacaan file .json dari angka `random_number` dan ditulis kedalam file txt dan dimasukkan kedalam direktori yang telah di pass


## d. Melakukan perulanagn dengan _primogems_

**Inisiasi Nilai Primogems dan melakukan perulangan**

```
int primogems = 79000;
int i = 1;

while(primogems>=160){
  primogems-=160;
  ...
  i++;
  sleep(1);
}        
```

## e. Menjalankan _script_ sesuai dengan tangggal

**Membuat struct Tanggal dan Waktu**

```
struct script_time {
  int month;
  int day;
  int hour;
  int minute;
  int second;
};  
```

**Menjalankan Script sesuai dengan tangggal dan waktu yang ditentukan**

```
// 30 Maret jam 04:44
struct script_time start;
start.month = 2;
start.day = 30;
start.hour = 4;
start.minute = 44;
start.second = 0;

// 30 Maret jam 07:44
struct script_time end;
end.month = 2;
end.day = 30;
end.hour = 7;
end.minute = 44;
end.second = 0;

if( (localtime(&now)->tm_mon == start.month) && (localtime(&now)->tm_mday == start.day) && (localtime(&now)->tm_hour == start.hour) && (localtime(&now)->tm_min == start.minute) && (localtime(&now)->tm_sec == start.second)){
  //do gacha here
}

if( (localtime(&now)->tm_mon == end.month) && (localtime(&now)->tm_mday == end.day) && (localtime(&now)->tm_hour == end.hour) && (localtime(&now)->tm_min == end.minute) && (localtime(&now)->tm_sec == end.second)){
  //zip gacha and delete folder
}
```

**Melakukan Zip File Gacha**

```
char *argv[]={"zip","-P","satuduatiga","-mr","not_safe_for_wibu.zip","gacha_gacha", NULL};
execv("/usr/bin/zip",argv);
```
 - penjelasan: 
disini kita menggunakan command `zip` untuk melakukan zip file
command `-P` utnuk memberikan password kepada file .zip
command `-mr` untuk menghapus folder yang telah di zip

**Menghapus Folder**

disini kita membuat fungsi
```
void delete_directory(char *directory){
  pid_t child_id = fork();
	int status;

	if (child_id < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id == 0) {
    	char *argv[] = {"rm","-r" ,directory, NULL};
    	execv("/bin/rm", argv);
	}
	else 
    {
    	while((wait(&status)) > 0);
    	return;
	}
}
```
 - penjelasan: 
disini kita menggunakan command `rm` untuk melakukan hapus
command `-r` untuk menghapus folder

 - eksekusi:
```
delete_directory("characters");
delete_directory("weapons");
```


**Menghapus File**

disini kita membuat fungsi
```
void delete_file(char *file){
  pid_t child_id = fork();
	int status;

	if (child_id < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id == 0) {
    	char *argv[] = {"rm",file, NULL};
    	execv("/bin/rm", argv);
	}
	else 
    {
    	while((wait(&status)) > 0);
    	return;
	}
}
```
 - penjelasan: 
disini kita menggunakan command `rm` untuk melakukan hapus file

 - eksekusi:
```
delete_file("characters.zip");
delete_file("weapons.zip");
```

## Hasil Demo Projek

https://drive.google.com/file/d/1jpebctfh2guerJup6W6tOqqnlgiBJMUH/view?usp=sharing


# Soal 2 (belum selesai)

## Fungsi Main
```
int main ()
{
//Sequence Counter
int seqcounter = 0;

//sequence 1
while (seqcounter == 0)
    {
        pid_t child_id;
        // Creating  child
        child_id = fork();
        int status;


        if (child_id < 0)
        {
            exit(EXIT_FAILURE); 
        }

        if (child_id == 0) 
        {
            chdir ("/home/hemakesha");
            make_directory("drakor");
            chdir ("/drakor");

            char filePath[] = "/home/hemakesha/";
            char fileName[] = "drakor.zip";
            char projectPath[] = "/home/hemakesha/drakor/";
            strcat(filePath,fileName);
            char *argv[] = {"unzip", filePath, "-d", projectPath, NULL};
            execv("/usr/bin/unzip",argv);
        }

        else 
        {

            while ((wait(&status)) > 0);
            chdir ("/home/hemakesha/drakor");
            delete_directory("coding");
            delete_directory("song");
            delete_directory("trash");
            seqcounter++;
        }
    }



while  (seqcounter == 1)
    {
        pid_t child_id_2;
        // Creating  child
        child_id_2 = fork();
        int status_2;

        if (child_id_2 < 0)
        {
            exit(EXIT_FAILURE); 
        }  

        if (child_id_2 == 0) 
        {
            chdir ("/home/hemakesha/drakor");
            make_directory("thriller");
            make_directory("action");
            make_directory("romance");
            make_directory("horror");
            make_directory("school");
            make_directory("comedy");
            make_directory("fantasy");
        }

        else
        {
            chdir ("/home/hemakesha/drakor");

            // Keep printing tokens while one of the
            // delimiters present in str[].
            //read through list
            //usestrtok to rename2
            seqcounter++;
            break;
        }  
    }

  return 0;
}
```

Fungsi main berfungsi untuk menjalankan fungsi-fungsi perintah
seperti menghapus directory dan membuat directory, fungsi main
menggunakan teknik looping while, supaya dapat membaca code dibawah `execv`

##menghapus directory

```
void delete_directory(char *directory)
{
    pid_t child_id = fork();
	int status;

	if (child_id < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id == 0) 
    {
    	char *argv[] = {"rm","-r" ,directory, NULL};
    	execv("/bin/rm", argv);
	}
	else 
    {
    	while((wait(&status)) > 0);
    	return;
	}
}
```

fungsi menghapus directory menggunakan execv untuk mengambil fungsi `rm` sebagai remove, dan
memiliki argv berisi `"rm"` untuk remove `"-r"` untuk menunjuk kedalam directory `directory` untuk menentukan directory yang
ingin dihapus, dan  NULL untuk mengakhiri argv


##membuat directory
```
void make_directory(char *directory){
    pid_t child_id_m = fork();
	int statuss;

	if (child_id_m < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id_m == 0) 
    {
    	char *argv[] = {"mkdir", "-p", directory, NULL};
        execv("/bin/mkdir", argv);

	}
	else 
    {
    	while((wait(&statuss)) > 0);
    	return;
	}
}
```
fungsi membuat directory menggunakan execv untuk mengambil fungsi `mkdir` untuk membuat directory, dan
memiliki argv berisi `"mkdir"` untuk make directory `"-p"` untuk security `directory` untuk menentukan directory yang
ingin dibuat, dan  NULL untuk mengakhiri argv
## list filename
```
void list_filename()
{
    int i = 0;
    char namafilm[6][20];
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d) 
    {
        while ((dir = readdir(d)) != NULL) 
        {


        }
        closedir(d);
    }
}
```

Fungsi file name berguna untuk men-list file name poster drakor untuk dimasukkan kedalam folder sesuai genre, tetapi fungsi belum
selesia dibuat

## meng-unzip directory drakor.zip
```
if (child_id == 0) 
        {
            chdir ("/home/hemakesha");
            make_directory("drakor");
            chdir ("/drakor");

            char filePath[] = "/home/hemakesha/";
            char fileName[] = "drakor.zip";
            char projectPath[] = "/home/hemakesha/drakor/";
            strcat(filePath,fileName);
            char *argv[] = {"unzip", filePath, "-d", projectPath, NULL};
            execv("/usr/bin/unzip",argv);
        }
```
fungsi if adalah sebuah child process yang berguna untuk meng-unzip folder atau 
directory zip drakor.zip, hal ini dicapai dengan cara: <br />
- membuat directory bernama "drakor" dengan memakai fungsi `make_directory`dan "drakor"
  sebagai input dalam fungsi
- setelah itu mengunzip directory zip "drakor.zip" dengan cara membuat `filePath` dan `fileName`
  dan menggabungkan keduanya menggunakan `strcat` untuk menentukan file apa yang akan di unzip, 
  serta menggunakan `filePath` sebagai `projectPath` untuk memasukkan file-file yang telah di extract
  kedalam folder yang sudah directory yang sudah ditentukan, yaitu "/home/hemakesha/drakor/"
- cara meng-unzip directory zip drakor.zip menggunakan execv yang menggunakan fungsi unzip, dengan
  argv `"unzip"` untuk unzip, `filePath` untuk menentukan file manayang di unzip, `"-d"` untuk menunjukkan
  sebuah directory, `projectPath` untuk menentukan lokasi penempatan file-file didalam directory yang sudah
  di unzip, dan `NULL` untuk mengakhiri fungsi.

## menghapus directory atau folder yang tidak dibutuhkan (coding, trash, dan song)
```
else 
        {

            while ((wait(&status)) > 0);
            chdir ("/home/hemakesha/drakor");
            delete_directory("coding");
            delete_directory("song");
            delete_directory("trash");
            seqcounter++;
        }
```
fungsi else adalah sebuah parent process yang berguna untuk menghapus directory didalam folder drakor yang tidak
dibutuhkan, hal ini dicapai dengan cara: <br />
- menunggu proses fungsi if selesai dengan ` while ((wait(&status)) > 0)` supaya folder coding, trash, dan song 
  muncul sebelum dihapus
- `chdir ("/home/hemakesha/drakor")` digunakan untuk mengubah directory ke directory atau folder drakor
- menghapus folder yang tidak dibutuhkan (coding, trash, dan song) dengan fungsi `delete_directory`
- `seqcounter++` digunakan untuk increment atau menambahkan value didalam variabel `seqcounter`
   agar dapat keluar dari while loop sequence pertama.


## membuat directory kategori genre drakor
```
if (child_id_2 == 0) 
        {
            chdir ("/home/hemakesha/drakor");
            make_directory("thriller");
            make_directory("action");
            make_directory("romance");
            make_directory("horror");
            make_directory("school");
            make_directory("comedy");
            make_directory("fantasy");
        }
```
fungsi if adalah sebuah child process yang berguna untuk membuat directory kategori genre drakor yang nantinya akan 
dimasukkan poster sesuai dengan genre-nya, hal ini dicapai dengan cara: <br />
- `chdir ("/home/hemakesha/drakor")` digunakan untuk mengubah directory ke directory atau folder drakor
- `make_directory("*directory")` merupakan fungsi yang berfungi untuk membuat directory genre drakor

## men-list nama-nama poster drakor untuk memasukkan poster kedalam folder genre sesuai dengan genre nya
```
else
        {
            chdir ("/home/hemakesha/drakor");

            // Keep printing tokens while one of the
            // delimiters present in str[].
            //read through list
            //usestrtok to rename2
            seqcounter++;
            break;
        }  
```
fungsi else adalah sebuah parent process yang berguna untuk men-list nama-nama poster drakor dan
memasukkan poster kedalam folder genre sesuai dengan genre nya, dapat dilihat bahwa fungsi belum
selesai dibuat.


# Soal 3

Setiap proses pada bagian soal akan dieksekusi pada proses child yang dibuat dengan menggunakan fungsi `fork()`

**Membuat direktori darat dan air**

Proses membuat direktory darat dan air

```
pid_t child_id_2;
		int status_2;
		child_id_2 = fork();

		if(child_id_2 < 0) exit(EXIT_FAILURE);

		if(child_id_2 == 0){
			char *argv[] = {"mkdir", "-p", "/home/james/modul2/darat", NULL};
                	execv("/bin/mkdir", argv);
		}
		else{
			while ((wait(&status_2)) > 0);
                	sleep(3);
                	char *argv[] = {"mkdir", "-p", "/home/james/modul2/air", NULL};
                	execv("/bin/mkdir", argv);
		}

```

- penjelasan :
	- pid_t child_id_2 merupakan variabel yang akan menampung nilai 0 (untuk child process) dan pid child process (untuk parent process)

- eksekusi : 

Proses child akan membuat direktori darat dengan menggunakan fungsi `execv()`
```
if(child_id_2 == 0){
			char *argv[] = {"mkdir", "-p", "/home/james/modul2/darat", NULL};
                	execv("/bin/mkdir", argv);
		}
```
Selanjutnya pada proses parent akan digunakan fungsi `wait()` untuk menunggu proses child selesai. Setelah itu, dilakukan jeda 3 detik dengan fungsi `sleep()` sesuai dengan ketentuan soal. Kemudian direktori air dibuat dengan fungsi `execv()` setelah jeda selesai.


**Mengekstrak `animal.zip`**

Proses mengekstrak `animal.zip`

```
while((wait(&status)) > 0);
		int status_3;
		pid_t child_id_3;
		child_id_3 = fork();
		if(child_id_3 < 0) exit(EXIT_FAILURE);

		if(child_id_3 == 0){
			char filePath[] = "./animal.zip";
			char pathUnzip[] = "/home/james/modul2";
			char *argv[] = {"unzip", filePath, "-d", pathUnzip, NULL};
			execv("/usr/bin/unzip", argv);
		}
```
- penjelasan :

	- Fungsi `wait()` yang berada di atas berguna untuk menunggu proses **Membuat direktori darat dan air** selesai.

	- int status_3 digunakan untuk fungsi `wait()`
	
	- pid_t child_id_3 digunakan untuk menampung nilai 0 untuk child ataupun pid child untuk parent process

- eksekusi :

Digunakan fungsi `fork()` untuk membuat proses child
```
child_id_3 = fork();
```

Bila gagal dalam membuat proses child, maka akan dijalankan fungsi `exit()` untuk menghentikan program.
```
if(child_id_3 < 0) exit(EXIT_FAILURE);
```

Proses child akan melakukan proses unzip file yang berada di `filePath` ke direktori `pathUnzip` yang dijalankan dengan fungsi `execv()`
```
if(child_id_3 == 0){
			char filePath[] = "./animal.zip";
			char pathUnzip[] = "/home/james/modul2";
			char *argv[] = {"unzip", filePath, "-d", pathUnzip, NULL};
			execv("/usr/bin/unzip", argv);
		}
```

**Memilah setiap file**

Berikut kode memilah setiap file

```
while((wait(&status_3)) > 0);
			DIR *folder;
			struct dirent *entry;

			folder = opendir("/home/james/modul2/animal");
			while ( (entry=readdir(folder)) ){
				if (fork() == 0){
					char fileName[300];
                                        snprintf(fileName, 300, "/home/james/modul2/animal/%s", entry->d_name);

					if ( strstr(entry->d_name, "darat" ) != NULL){
						char *argv[] = {"move", fileName, "/home/james/modul2/darat", NULL};
						execv("/usr/bin/mv", argv);
                                	}
					else if (strstr(entry->d_name, "air") != NULL){
						char *argv[] = {"move", fileName, "/home/james/modul2/air", NULL};
						execv("/usr/bin/mv", argv);
					}
					else{
						if(is_file(fileName) == 1){
							char *argv[] = {"remove", fileName, NULL};
                                                	execv("/usr/bin/rm", argv);
						}
					}

				}
			}
			closedir(folder);
```

- penjelasan:
	- DIR *folder digunakan untuk menampung folder yang ingin diakses

	- struct dirent *entry digunakan untuk membantu isi dari direktori yang diakses

- eksekusi:

Untuk memilah-milah file yang berada direktori animal, kita perlu mengakses direktori tersebut dan membaca file yang berada di direktori.

```
DIR *folder;
			struct dirent *entry;

			folder = opendir("/home/james/modul2/animal");
			while ( (entry=readdir(folder)) ){...}
```

Lakukan pembuatan proses child setiap membaca file. Kemudian simpan setiap path file pada variabel `fileName`

```
if (fork() == 0){
					char fileName[300];
                                        snprintf(fileName, 300, "/home/james/modul2/animal/%s", entry->d_name);

...
}
```

Lakukan percabangan untuk memilah file berdasarkan kecocokan stringnya menggunakan fungsi `strstr()`. Jika nama file (`entry->d_name`) mengandung kata darat, maka file akan dipindahkan ke direktori darat dengan fungsi `exec()`, jika mengandung kata air, akan dipindahkan ke direktori air.

```
if ( strstr(entry->d_name, "darat" ) != NULL){
						char *argv[] = {"move", fileName, "/home/james/modul2/darat", NULL};
						execv("/usr/bin/mv", argv);
                                	}
					else if (strstr(entry->d_name, "air") != NULL){
						char *argv[] = {"move", fileName, "/home/james/modul2/air", NULL};
						execv("/usr/bin/mv", argv);
					}
```

Jika tidak mengandung darat dan air, maka jalankan perintah yang menghapus file tersebut. Tetapi, dicek terlebih dahulu apakah yang dibaca saat ini adalah file atau direktori (baik itu direktori parent ataupun direktori saat ini) dengan menggunakan fungsi `is_file()`, agar tidak terjadi error.

Berikut fungsi `is_file()`
```
int is_file(const char*path){
	struct stat path_stat;
	stat(path, &path_stat);
	return S_ISREG(path_stat.st_mode);
}
```

Berikut perintah menghapus file
```
else{
						if(is_file(fileName) == 1){
							char *argv[] = {"remove", fileName, NULL};
                                                	execv("/usr/bin/rm", argv);
						}
					}
```

Kemudian tutup direktori yang diakses
```
closedir(folder);
```

**Menghapus semua file dengan nama burung**

Berikut kode untuk menghapus file burung
```
DIR *dir;
struct dirent *ent;

dir = opendir("/home/james/modul2/darat");
while ( (ent=readdir(dir)) ){
				if(fork() == 0){
					char fileName[300];
					snprintf(fileName, 300, "/home/james/modul2/darat/%s", ent->d_name);
					if ( strstr(ent->d_name, "bird" ) != NULL){
                                                if(is_file(fileName) == 1){
                                                        char *argv[] = {"remove", fileName, NULL};
                                                        execv("/usr/bin/rm", argv);
                                                }

                                        }

				}
}
closedir(dir);
```
- penjelasan :
	- DIR *dir digunakan untuk menampung folder yang akan diakses

	- struct dirent *ent digunakan unutk membantu mengakses isi dari direktori yang diakses

- eksekusi :

Akses terlebih dahulu direktori dimana file burung berada, yaitu direktori darat. Setelah itu, lakukan perulangan untuk membaca semua file yang berada pada direktori darat. 
```
DIR *dir;
			struct dirent *ent;

			dir = opendir("/home/james/modul2/darat");
			while ( (ent=readdir(dir)) ){...}
```

Buat proses child baru setiap membaca file. Kemudian buat variabel yang akan digunakan untuk menamping path atau lokasi file berada.
```
if(fork() == 0){
					char fileName[300];
					snprintf(fileName, 300, "/home/james/modul2/darat/%s", ent->d_name);

...
}
```

Lakukan pengecekan untuk memilih file yang mengandung string `bird`, kemudian menghapusnya. Tetapi sebelum menghapus file, lakukan pengecekan dengan fungsi `is_file()` untuk memastikan yang saat ini dibaca adalah file, bukan direktori.
```
if ( strstr(ent->d_name, "bird" ) != NULL){
                                                if(is_file(fileName) == 1){
                                                        char *argv[] = {"remove", fileName, NULL};
                                                        execv("/usr/bin/rm", argv);
                                                }
```

Terakhir, tutup direktori yang telah diakses
```
closedir(dir);
```

**Membuat file `list.txt`**

File list.txt merupakan file mencatat semua nama file yang berada pada direktori air dengan format UID_[UID file permission]_Nama File.[jpg/png]. Berikut kode untuk membuat file `list.txt`
```
struct dirent *ep;
FILE *fptr;
DIR *dp;
uid_t uid;
int r;
char dpath[100] = "/home/james/modul2/air";
pid_t child_id_4;
int status_4;
child_id_4 = fork();

			if(child_id_4 == 0){
				char *argv[] = {"touch", "/home/james/modul2/air/list.txt", NULL};
				execv("/usr/bin/touch", argv);
			}
			else {
				while((wait(&status_4)) > 0);
				fptr = fopen("/home/james/modul2/air/list.txt", "w");
				dp = opendir(dpath);

				if (fptr == NULL){
					printf("File doesn't exist\n");
				}

				if (dp != NULL){
					while((ep = readdir(dp)) != NULL){
						struct stat info;
						char pathFile[100];
						char name[100] = "";

						strcpy(pathFile, dpath);
						strcat(pathFile, "/");
						strcat(pathFile, ep->d_name);

						if(is_file(pathFile) == 1){
						
							r = stat(pathFile, &info);
							if(r == -1){
								fprintf(stderr, "File error\n");
								exit(1);
							}

							struct passwd *pw = getpwuid(info.st_uid);

							strcat(name, pw->pw_name);
							strcat(name, "_");

							if(info.st_mode & S_IRUSR)
								strcat(name, "r");
							if(info.st_mode & S_IWUSR)
								strcat(name, "w");
							if(info.st_mode & S_IXUSR)
								strcat(name, "x");

							strcat(name, "_");
							strcat(name, ep->d_name);
							fprintf(fptr, "%s\n", name);
						}
					}
				}
			}
			closedir(dp);
			fclose(fptr);
```

- perintah :
	- FILE *fptr digunakan untuk menampung file yang ingin diakses

	- 
- eksekusi :

Gunakan fungsi `fork()` untuk membuat proses child. Proces child digunakan membuat file `list.txt`
```
if(child_id_4 == 0){
				char *argv[] = {"touch", "/home/james/modul2/air/list.txt", NULL};
				execv("/usr/bin/touch", argv);
			}
```

pada proses parent, gunakan akan diakses file `list.txt` yang telah dibuat dan akses juga direktori `air`
```
else {
				while((wait(&status_4)) > 0);
				fptr = fopen("/home/james/modul2/air/list.txt", "w");
				dp = opendir(dpath);

				if (fptr == NULL){
					printf("File doesn't exist\n");
				}

				...
}
```

Setelah berhasil mengakses direktori, tampung path atau lokasi setiap file di direktori air pada variabel `pathFile`. Jika file tidak error, maka gunakan fungsi `getpwuid(info.st_uid)` untuk memperoleh uid. Nilai kembalian tersebut ditampung pada struct passwd *pw. Setelah mendapatkan uid, simpan uid ke variabel `name` kemudian tambahkan karakter `_` setelahnya.
```
if (dp != NULL){
					while((ep = readdir(dp)) != NULL){
						struct stat info;
						char pathFile[100];
						char name[100] = "";

						strcpy(pathFile, dpath);
						strcat(pathFile, "/");
						strcat(pathFile, ep->d_name);

						if(is_file(pathFile) == 1){
						
							r = stat(pathFile, &info);
							if(r == -1){
								fprintf(stderr, "File error\n");
								exit(1);
							}

							struct passwd *pw = getpwuid(info.st_uid);

							strcat(name, pw->pw_name);
							strcat(name, "_");

							...
						}
					}
}
```

Lakukan percabangan untuk mengecek file permission. Setiap file permission yang memenuhi, akan dimasukkan karakter ke variabel `name`. Kemudian tambahkan karakter `_` pada variabel `name`
```
if(info.st_mode & S_IRUSR)
	strcat(name, "r");
if(info.st_mode & S_IWUSR)
	strcat(name, "w");
if(info.st_mode & S_IXUSR)
	strcat(name, "x");

strcat(name, "_");

```

Kemudian tambahkan nama file pada variabel `name` dan masukkan variabel `name` ke file `list.txt`
```
strcat(name, ep->d_name);
fprintf(fptr, "%s\n", name);

```

Setelah itu, tutup direktori dan file yang diakses
```
closedir(dp);
fclose(fptr);
```

#kesulitan
-membuat list untuk men-list filename
-mengetahui membaca isi direktori ternyata terikut membaca direktori parent, sehingga kesulitan saat menghapus isi direktori sesuai ketentuan soal


