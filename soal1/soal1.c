#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <wait.h>
#include <dirent.h>
#include <json-c/json.h>

char *drive_character[] = {"https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp","characters.zip"};
char *drive_weapon[] = {"https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "weapons.zip"};

char characters_file[500][500];
int characters_count=0;

char weapons_file[500][500];
int weapons_count=0;


struct script_time {
  int month;
  int day;
  int hour;
  int minute;
  int second;
};

void read_characters_directory(){
    characters_count=0;
    DIR *directory_a;
    struct dirent *en_a;
    directory_a = opendir("characters");
    while ((en_a = readdir(directory_a)) != NULL) {
      strcpy(characters_file[characters_count],en_a->d_name);
      characters_count++;
    }
    closedir(directory_a);
}

void read_weapons_directory(){
    DIR *directory;
    struct dirent *en;
    directory = opendir("weapons");
    while ((en = readdir(directory)) != NULL) { 
        strcpy(weapons_file[weapons_count],en->d_name);
        weapons_count++;
    }
    closedir(directory);
}

void get_characters_gacha(int random_number, int gacha_count, int remain_primogems, char *nama_folder, char *nama_file){
  FILE *f;
  char buffer[1000000];

  struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *rarity;

  char directory[100] = "characters/";
  strcat(directory,characters_file[random_number]);

  f = fopen(directory,"r");
  fread(buffer, 1000000, 1, f);
  fclose(f);
  parsed_json = json_tokener_parse(buffer);
  json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);


  char write_directory[500];
  strcpy(write_directory,"gacha_gacha");
  strcat(write_directory,"/");
  strcat(write_directory,nama_folder);
  strcat(write_directory,"/");
  strcat(write_directory,nama_file);
  strcat(write_directory, ".txt");
  
  FILE *txt = fopen(write_directory, "a");
  fprintf(txt, "%d_characters_%d_%s_%d\n",gacha_count, json_object_get_int(rarity), json_object_get_string(name), remain_primogems);
  fclose(txt);
}

void get_weapons_gacha(int random_number, int gacha_count, int remain_primogems, char *nama_folder, char *nama_file){
  FILE *f;
  char buffer[5000000];

  struct json_object *parsed_json;
	struct json_object *name;
	struct json_object *rarity;

  char directory[100] = "weapons/";
  strcat(directory,weapons_file[random_number]);

  f = fopen(directory,"r");
  fread(buffer, 5000000, 1, f);
  fclose(f);
  parsed_json = json_tokener_parse(buffer);
  json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);

  char write_directory[500];
  strcpy(write_directory,"gacha_gacha");
  strcat(write_directory,"/");
  strcat(write_directory,nama_folder);
  strcat(write_directory,"/");
  strcat(write_directory,nama_file);
  strcat(write_directory, ".txt");
  
  FILE *txt = fopen(write_directory, "a");
  fprintf(txt, "%d_weapons_%d_%s_%d\n",gacha_count, json_object_get_int(rarity), json_object_get_string(name), remain_primogems);
  fclose(txt);
}

void make_folder_gacha(char *folder_name)
{
	pid_t child_id = fork();
	int status;

	if (child_id < 0) {
      	exit(EXIT_FAILURE);
	}
	if (child_id == 0) {
    char directory_name[200];
    strcpy(directory_name, "gacha_gacha");
    strcat(directory_name, "/");
    strcat(directory_name, folder_name);
   	char *argv[] = {"mkdir", "-p", directory_name, NULL};
   	execv("/bin/mkdir", argv);
	}
	else {
    	while((wait(&status)) > 0);
    	return;
	}
}



void make_directory(char *directory){
  char *argv[] = {"mkdir", "-p", directory, NULL};
  execv("/bin/mkdir", argv);
}

void delete_file(char *file){
  pid_t child_id = fork();
	int status;

	if (child_id < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id == 0) {
    	char *argv[] = {"rm",file, NULL};
    	execv("/bin/rm", argv);
	}
	else 
    {
    	while((wait(&status)) > 0);
    	return;
	}
}

void delete_directory(char *directory){
  pid_t child_id = fork();
	int status;

	if (child_id < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id == 0) {
    	char *argv[] = {"rm","-r" ,directory, NULL};
    	execv("/bin/rm", argv);
	}
	else 
    {
    	while((wait(&status)) > 0);
    	return;
	}
}

void download_and_zip (char *source, char *projectPath, char *fileName){
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    char filePath[150];
    strcpy(filePath,projectPath);
    strcat(filePath,fileName);
    char *argv[] = {"wget","--no-check-certificate", source, "-O", filePath, NULL};
    execv("/usr/bin/wget",argv);
  } else {
    while ((wait(&status)) > 0);
    char filePath[150];
    strcpy(filePath,projectPath);
    strcat(filePath,fileName);
    char *argv[] = {"unzip", filePath, "-d", projectPath, NULL};
    execv("/usr/bin/unzip",argv);
  }
}

void download_data(char *projectPath){
  pid_t download_id;
  int status_download;
  download_id = fork();
  if (download_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }
  if (download_id == 0) {
    download_and_zip(drive_character[0],projectPath,drive_character[1]);
  } else {
    while ((wait(&status_download)) > 0);
    download_and_zip(drive_weapon[0],projectPath,drive_weapon[1]);
  }
}

void extract_file(char *data[]){
  char *argv[] = {"unzip", data[1], NULL};
  execv("/usr/bin/unzip", argv);
}


int main(){
  pid_t process_id = 0;
  pid_t sid = 0;
  process_id = fork();

  // CHILD PROCESS FAIL AND KILL PARENT PROCESS
  if (process_id < 0 || process_id > 0) {
    exit(1);
  }

  //UNMASK
  umask(0);
  sid = setsid();
  if (sid < 0) {
    exit(1);
  }

  // CURRENT WORK DIR
  char projectPath[80];
  strcpy(projectPath, "/home/wina/Documents/soal-shift-sisop-modul-2-a03-2022/soal1/");
  chdir(projectPath);
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
 

  while(1){
    sleep(1);
    
    time_t now;
    struct tm *now_tm;
    now = time(NULL);

    // 30 Maret jam 04:44
    struct script_time start;
    start.month = 2;
    start.day = 30;
    start.hour = 4;
    start.minute = 44;
    start.second = 0;

    // 30 Maret jam 07:44
    struct script_time end;
    end.month = 2;
    end.day = 30;
    end.hour = 7;
    end.minute = 44;
    end.second = 0;

    

    if( (localtime(&now)->tm_mon == start.month) && (localtime(&now)->tm_mday == start.day) && (localtime(&now)->tm_hour == start.hour) && (localtime(&now)->tm_min == start.minute) && (localtime(&now)->tm_sec == start.second)){
      pid_t child_id;
      int status;

      child_id = fork();

      if (child_id < 0) {
        exit(EXIT_FAILURE);
      }
      if (child_id == 0) {
        make_directory("gacha_gacha");
      }
      else {
        pid_t child_id_1;
        int status_1;

        child_id_1 = fork();

        if (child_id_1 < 0) {
          exit(EXIT_FAILURE);
        }
        if (child_id_1 == 0) {
          download_data(projectPath);
        } else {
          while ((wait(&status_1)) > 0);
          read_weapons_directory();
          read_characters_directory();
          
          

          char nama_folder[200];
          char nama_file[200];

          int file_num = 0;
          int folder_num = 0;

          int primogems = 79000;

          int i = 1;
          int r;

          
          while(primogems>=160){
            primogems-=160;
            

            if(i%10==1){
              file_num+=10;
              snprintf(nama_file, 200, "%d:%d:%d_gacha_%d", localtime(&now)->tm_hour, localtime(&now)->tm_min, localtime(&now)->tm_sec, file_num); 
            } 
            if(i%90==1){
              folder_num+=90;
              snprintf(nama_folder, 200, "total_gacha_%d", folder_num); 
              make_folder_gacha(nama_folder);
            }
            

            if(i%2==0){
              r = rand() % weapons_count;
              get_weapons_gacha(r,i,primogems, nama_folder, nama_file);
            } else {
              r = rand() % characters_count;
              get_characters_gacha(r,i,primogems, nama_folder, nama_file);
            }
            i++;
            sleep(1);
          }
        }
      }
    } 
    
    if( (localtime(&now)->tm_mon == end.month) && (localtime(&now)->tm_mday == end.day) && (localtime(&now)->tm_hour == end.hour) && (localtime(&now)->tm_min == end.minute) && (localtime(&now)->tm_sec == end.second)){
        pid_t child_id_2;
        int status_2;
        printf("halo");
        child_id_2 = fork();

        if (child_id_2 < 0) {
          exit(EXIT_FAILURE);
        }
        if (child_id_2 == 0) {
          char *argv[]={"zip","-P","satuduatiga","-mr","not_safe_for_wibu.zip","gacha_gacha", NULL};
          execv("/usr/bin/zip",argv);
        }
        else {
          while ((wait(&status_2)) > 0);
          delete_directory("characters");
          delete_directory("weapons");
          delete_file("characters.zip");
          delete_file("weapons.zip");
        }
    }
  }

  return 0;
}

