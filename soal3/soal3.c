#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>//for fork
#include <stdlib.h>//for exit
#include <unistd.h>//for fork, execv
#include <wait.h>
#include <dirent.h>//for dir
#include <string.h>
#include <pwd.h>//uid
#include <grp.h>//uid

int is_file(const char*path){
	struct stat path_stat;
	stat(path, &path_stat);
	return S_ISREG(path_stat.st_mode);
}

int main(){
	pid_t child_id;
	int status;

	child_id = fork();

	if(child_id < 0){
		exit(EXIT_FAILURE);
	}

	//a
	if(child_id == 0){
		pid_t child_id_2;
		int status_2;
		child_id_2 = fork();

		if(child_id_2 < 0) exit(EXIT_FAILURE);

		if(child_id_2 == 0){
			char *argv[] = {"mkdir", "-p", "/home/james/modul2/darat", NULL};
                	execv("/bin/mkdir", argv);
		}
		else{
			while ((wait(&status_2)) > 0);
                	sleep(3);
                	char *argv[] = {"mkdir", "-p", "/home/james/modul2/air", NULL};
                	execv("/bin/mkdir", argv);
		}
	}
	else{
		//b
		while((wait(&status)) > 0);
		int status_3;
		pid_t child_id_3;
		child_id_3 = fork();
		if(child_id_3 < 0) exit(EXIT_FAILURE);

		if(child_id_3 == 0){
			char filePath[] = "./animal.zip";
			char pathUnzip[] = "/home/james/modul2";
			char *argv[] = {"unzip", filePath, "-d", pathUnzip, NULL};
			execv("/usr/bin/unzip", argv);
		}
		else{
			//c
			while((wait(&status_3)) > 0);
			DIR *folder;
			struct dirent *entry;

			folder = opendir("/home/james/modul2/animal");
			while ( (entry=readdir(folder)) ){
				if (fork() == 0){
					char fileName[300];
                                        snprintf(fileName, 300, "/home/james/modul2/animal/%s", entry->d_name);

					if ( strstr(entry->d_name, "darat" ) != NULL){
						char *argv[] = {"move", fileName, "/home/james/modul2/darat", NULL};
						execv("/usr/bin/mv", argv);
                                	}
					else if (strstr(entry->d_name, "air") != NULL){
						char *argv[] = {"move", fileName, "/home/james/modul2/air", NULL};
						execv("/usr/bin/mv", argv);
					}
					else{
						if(is_file(fileName) == 1){
							char *argv[] = {"remove", fileName, NULL};
                                                	execv("/usr/bin/rm", argv);
						}
					}

				}
			}
			closedir(folder);

			//d
			DIR *dir;
			struct dirent *ent;

			dir = opendir("/home/james/modul2/darat");
			while ( (ent=readdir(dir)) ){
				if(fork() == 0){
					char fileName[300];
					snprintf(fileName, 300, "/home/james/modul2/darat/%s", ent->d_name);
					if ( strstr(ent->d_name, "bird" ) != NULL){
                                                if(is_file(fileName) == 1){
                                                        char *argv[] = {"remove", fileName, NULL};
                                                        execv("/usr/bin/rm", argv);
                                                }

                                        }

				}
			}
			closedir(dir);

			// e
			struct dirent *ep;
			FILE *fptr;
			DIR *dp;

			//struct passwd *p;
			uid_t uid;

			//struct stat info;
			int r;

			char dpath[100] = "/home/james/modul2/air";

			pid_t child_id_4;
			int status_4;
			child_id_4 = fork();

			if(child_id_4 == 0){
				char *argv[] = {"touch", "/home/james/modul2/air/list.txt", NULL};
				execv("/usr/bin/touch", argv);
			}
			else {
				while((wait(&status_4)) > 0);
				fptr = fopen("/home/james/modul2/air/list.txt", "w");
				dp = opendir(dpath);

				if (fptr == NULL){
					printf("File doesn't exist\n");
				}

				if (dp != NULL){
					while((ep = readdir(dp)) != NULL){
						struct stat info;
						char pathFile[100];
						char name[100] = "";

						strcpy(pathFile, dpath);
						strcat(pathFile, "/");
						strcat(pathFile, ep->d_name);

						if(is_file(pathFile) == 1){
						
							r = stat(pathFile, &info);
							if(r == -1){
								fprintf(stderr, "File error\n");
								exit(1);
							}

							struct passwd *pw = getpwuid(info.st_uid);

							strcat(name, pw->pw_name);
							strcat(name, "_");

							if(info.st_mode & S_IRUSR)
								strcat(name, "r");
							if(info.st_mode & S_IWUSR)
								strcat(name, "w");
							if(info.st_mode & S_IXUSR)
								strcat(name, "x");

							strcat(name, "_");
							strcat(name, ep->d_name);
							fprintf(fptr, "%s\n", name);
						}
					}
				}
			}
			closedir(dp);
			fclose(fptr);
		}

	}

}
